package com.dksgames;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Main {

	private static String res_dir = "res";
	private static String out_dir = "out";
	
	private static int cell_width = 0;
	private static int cell_hight = 0;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		if(args.length > 1) {
			cell_width = Integer.parseInt(args[0]);
			cell_hight = Integer.parseInt(args[1]);
		}else {
			cell_width = 64;
			cell_hight = 47;
		}
		
		if(cell_width * cell_hight == 0) {
			System.err.println("not input cell_width and cell_hight");
			return;
		}
		
		File resDir = new File("." + File.separator + res_dir);
		if(!resDir.exists())resDir.mkdirs();
		try {
			del("." + File.separator + out_dir);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		File outDir = new File("." + File.separator + out_dir);
		if(!outDir.exists())outDir.mkdirs();
		
		File file = new File("." + File.separator + res_dir);
		for (String fileName : file.list()) {
			if (!fileName.contains(".png")) {
				continue;
			}
			BufferedImage src = null;
			File imgFile = new File("." + File.separator + res_dir + File.separator + fileName);
			try {
				src = ImageIO.read(imgFile);
				if(src != null) {
					int width = src.getWidth(null);
					int height = src.getHeight(null);
	
					int req_row = Math.abs(height -1) / cell_hight + 1;
					int req_column = Math.abs(width -1) / cell_width + 1;
					
					File outImage = new File("." + File.separator + out_dir + File.separator + req_column + "_" + req_row + "_" + fileName);
					copyFile(imgFile, outImage);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				
			}
		}
	}
	
    // 复制文件
    public static void copyFile(File sourceFile, File targetFile) throws IOException {

    	if(!sourceFile.exists()){
			System.err.println("sourceFile nor exist : " + sourceFile.getName());
			return;
    	}
    	
    	if(!targetFile.exists())targetFile.createNewFile();
    	
        BufferedInputStream inBuff = null;
        BufferedOutputStream outBuff = null;
        try {
            // 新建文件输入流并对它进行缓冲
            inBuff = new BufferedInputStream(new FileInputStream(sourceFile));

            // 新建文件输出流并对它进行缓冲
            outBuff = new BufferedOutputStream(new FileOutputStream(targetFile));

            // 缓冲数组
            byte[] b = new byte[1024 * 5];
            int len;
            while ((len = inBuff.read(b)) != -1) {
                outBuff.write(b, 0, len);
            }
            // 刷新此缓冲的输出流
            outBuff.flush();
        } finally {
            // 关闭流
            if (inBuff != null)
                inBuff.close();
            if (outBuff != null)
                outBuff.close();
        }
    }
    
    /**
     * 
     * @param filepath
     * @throws IOException
     */
    public static void del(String filepath) throws IOException {
        File f = new File(filepath);// 定义文件路径
        if (f.exists() && f.isDirectory()) {// 判断是文件还是目录
            if (f.listFiles().length == 0) {// 若目录下没有文件则直接删除
                f.delete();
            } else {// 若有则把文件放进数组，并判断是否有下级目录
                File delFile[] = f.listFiles();
                int i = f.listFiles().length;
                for (int j = 0; j < i; j++) {
                    if (delFile[j].isDirectory()) {
                        del(delFile[j].getAbsolutePath());// 递归调用del方法并取得子目录路径
                    }
                    delFile[j].delete();// 删除文件
                }
            }
        }
    }

}
