package com.dksgames;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import javax.imageio.ImageIO;

public class Main {
	
	private static String res_dir = "res";
	private static String out_dir = "out";
	
	private static int image_width = 2048;
	private static int image_hight = 2048;
	
	private static int cell_width = 0;
	private static int cell_hight = 0;
	
	private static int count_width = 0;
	private static int count_hight = 0;
	
	private static HashMap<Integer, Integer> imagemap = new HashMap<Integer, Integer>();

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		if(args.length > 1) {
			cell_width = Integer.parseInt(args[0]);
			cell_hight = Integer.parseInt(args[1]);
		}else {
			cell_width = 64;
			cell_hight = 47;
		}
		
		if(cell_width * cell_hight == 0) {
			System.err.println("not input cell_width and cell_hight");
			return;
		}
		
		File resDir = new File("." + File.separator + res_dir);
		if(!resDir.exists())resDir.mkdirs();
		File outDir = new File("." + File.separator + out_dir);
		if(!outDir.exists())outDir.mkdirs();

		int row = image_hight / cell_hight;
		int column = image_width / cell_width;
		
		BufferedImage outImg = new BufferedImage(image_width, image_hight, BufferedImage.TYPE_4BYTE_ABGR);
		
		File file = new File("." + File.separator + res_dir);
		for (String fileName : file.list()) {
			if (!fileName.contains(".png")) {
				continue;
			}
			BufferedImage src = null;
			File imgFile = new File("." + File.separator + res_dir + File.separator + fileName);
			try {
				src = ImageIO.read(imgFile);
				if(src != null) {
					int width = src.getWidth(null);
					int height = src.getHeight(null);
	
					int req_row = Math.abs(height -1) / cell_hight + 1;
					int req_column = Math.abs(width -1) / cell_width + 1;
	
					int find_x = -1;
					int find_y = -1;
	
					boolean foundOne = false;
					
					found:
					for (int i = 0; i < (row - req_row); i++) {
						for (int j = 0; j < (column - req_column); j++) {
							Integer hasValue = imagemap.get(i*column+j);
							if(hasValue == null) {
								foundExist:
								for (int k = 0; k < req_row; k++) {
									for (int k2 = 0; k2 < req_column; k2++) {
										Integer hasExt = imagemap.get((i+k)*column+(j+k2));
										if(hasExt == null) {
											foundOne = true;
										}else {
											foundOne = false;
											break foundExist;
										}
									}
								}
								if(foundOne) {
									find_x = j;
									find_y = i;
									break found;
								}
							}
						}
					}
					
					if(foundOne) {
						int off_x = (cell_width * req_column - width) / 2;
						int off_y = (cell_hight * req_row - height) / 2;
						outImg.getGraphics().drawImage(src, find_x * cell_width + off_x, find_y * cell_hight + off_y, width, height, null);
	
						for (int i = find_y; i < find_y + req_row; i++) {
							for (int j = find_x; j < find_x + req_column; j++) {
								imagemap.put(i*column+j, 1);
							}
						}
					}else {
						System.err.println("err to found empty sets : " + fileName);
					}
					
				}else {
					System.err.println("err to read img : " + fileName);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				
			}
		};
		
		try {
			File mergeFile = new File("." + File.separator + out_dir + File.separator + "merge.png");
			if(!mergeFile.exists())mergeFile.createNewFile();
			ImageIO.write(outImg, "png", mergeFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   
	}

}
