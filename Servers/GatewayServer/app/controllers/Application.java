package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import service.CommandHandler;
import play.mvc.Controller;
import play.mvc.WebSocket;

public class Application extends Controller {

    /**
     * Handle the chat websocket.
     */
    public static WebSocket<JsonNode> index() {
        return new WebSocket<JsonNode>() {
            
            // Called when the Websocket Handshake is done.
            public void onReady(WebSocket.In<JsonNode> in, WebSocket.Out<JsonNode> out){
                
                // Join the chat room.
                try { 
                    CommandHandler.join(in, out);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };
    }
  
}
