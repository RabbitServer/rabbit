package client;

import akka.actor.UntypedActor;
import org.eclipse.jetty.websocket.WebSocket;
import org.eclipse.jetty.websocket.WebSocket.Connection;
import org.eclipse.jetty.websocket.WebSocketClient;
import org.eclipse.jetty.websocket.WebSocketClientFactory;

import java.net.URI;
import java.util.ArrayList;

/**
 * Created by Administrator on 2014/7/24.
 */
public class GatewayClient extends UntypedActor{

    ArrayList<Connection> mClientList = new ArrayList<Connection>();

    @Override
    public void onReceive(Object message) throws Exception {
        WebSocketClientFactory factory =  new WebSocketClientFactory();
        WebSocketClient client = factory.newWebSocketClient();
        Connection connect = (Connection) client.open(new URI("ws://172.16.1.66:7080"), new WebSocket() {
            @Override
            public void onOpen(Connection connection) {

            }

            @Override
            public void onClose(int i, String s) {

            }
        });
        mClientList.add(connect);
    }
}
