package controllers;

import akka.actor.ActorRef;
import akka.actor.Props;
import client.GatewayClient;
import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Akka;
import play.mvc.Controller;
import play.mvc.WebSocket;
import scala.concurrent.duration.Duration;
import service.AuthActor;

public class Application extends Controller {

    /**
     * Handle the chat websocket.
     */
    public static WebSocket<JsonNode> index() {
        return new WebSocket<JsonNode>() {
            
            // Called when the Websocket Handshake is done.
            public void onReady(WebSocket.In<JsonNode> in, WebSocket.Out<JsonNode> out){
                
                // call the realm.
                try { 
                    AuthActor.realm(in, out);
                    ActorRef instance = Akka.system().actorOf(Props.create(GatewayClient.class));
                    Akka.system().scheduler().scheduleOnce(Duration.Zero(), instance, "CLIENT", Akka.system().dispatcher(), null);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };
    }
  
}
