package models;

import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * Created by Administrator on 2014/7/4.
 */
@Entity
@Table(name="account" , schema="realmd")
public class Account extends Model{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @Constraints.Required
    @Formats.NonEmpty
    private String username;

    @Constraints.Required
    private String sha_pass_hash;

    @Constraints.Required
    private String password;

    @Constraints.Required
    private int gmlevel;

    @Constraints.Required
    private String sessionkey;

    @Constraints.Required
    private String v;

    @Constraints.Required
    private String s;

    @Constraints.Required
    private String email;

    @Constraints.Required
    private Long joindate;

    @Constraints.Required
    private String last_ip;

    @Constraints.Required
    private int failed_logins;

    @Constraints.Required
    private int locked;

    @Constraints.Required
    private Long last_login;

    @Constraints.Required
    private int active_realm_id;

    @Constraints.Required
    private int expansion;

    @Constraints.Required
    private Long mutetime;

    @Constraints.Required
    private int locale;

    /**
     * Generic query helper for entity Company with id Long
     */
    public static Model.Finder<Long,Account> find = new Model.Finder<Long,Account>(Long.class, Account.class);

    public static Map<String,String> options() {
        LinkedHashMap<String,String> options = new LinkedHashMap<String,String>();
        for(Account c: Account.find.orderBy("username").findList()) {
            options.put(c.id.toString(), c.username);
        }
        return options;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSha_pass_hash() {
        return sha_pass_hash;
    }

    public void setSha_pass_hash(String sha_pass_hash) {
        this.sha_pass_hash = sha_pass_hash;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getGmlevel() {
        return gmlevel;
    }

    public void setGmlevel(int gmlevel) {
        this.gmlevel = gmlevel;
    }

    public String getSessionkey() {
        return sessionkey;
    }

    public void setSessionkey(String sessionkey) {
        this.sessionkey = sessionkey;
    }

    public String getV() {
        return v;
    }

    public void setV(String v) {
        this.v = v;
    }

    public String getS() {
        return s;
    }

    public void setS(String s) {
        this.s = s;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getJoindate() {
        return joindate;
    }

    public void setJoindate(Long joindate) {
        this.joindate = joindate;
    }

    public String getLast_ip() {
        return last_ip;
    }

    public void setLast_ip(String last_ip) {
        this.last_ip = last_ip;
    }

    public int getFailed_logins() {
        return failed_logins;
    }

    public void setFailed_logins(int failed_logins) {
        this.failed_logins = failed_logins;
    }

    public int getLocked() {
        return locked;
    }

    public void setLocked(int locked) {
        this.locked = locked;
    }

    public Long getLast_login() {
        return last_login;
    }

    public void setLast_login(Long last_login) {
        this.last_login = last_login;
    }

    public int getActive_realm_id() {
        return active_realm_id;
    }

    public void setActive_realm_id(int active_realm_id) {
        this.active_realm_id = active_realm_id;
    }

    public int getExpansion() {
        return expansion;
    }

    public void setExpansion(int expansion) {
        this.expansion = expansion;
    }

    public Long getMutetime() {
        return mutetime;
    }

    public void setMutetime(Long mutetime) {
        this.mutetime = mutetime;
    }

    public int getLocale() {
        return locale;
    }

    public void setLocale(int locale) {
        this.locale = locale;
    }
}
