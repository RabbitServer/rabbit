package service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import eun.AuthCodes;
import eun.ErrorCodes;
import models.Account;
import play.Logger;
import play.libs.F;
import play.libs.F.Callback;
import play.libs.Json;
import play.mvc.WebSocket;

/**
 * Created by Administrator on 2014/7/4.
 */
public class AuthSocket implements Callback<JsonNode> {
    public Long id;
    public Account account;
    public WebSocket.Out<JsonNode> channel;

    public AuthSocket(WebSocket.Out<JsonNode> channel) {
        this.id = new Long(0);
        this.channel = channel;
        this.account = null;
    }

    @Override
    public void invoke(JsonNode jsonNode) throws Throwable {
        boolean resultOK = false;
        JsonNode cmd = jsonNode.get("cmd");
        if(cmd != null && cmd.isInt()) {
            switch (cmd.asInt())
            {
                case AuthCodes.eAuthCmd.CMD_AUTH_LOGON_CHALLENGE:
                    resultOK = _HandleLogonChallenge(jsonNode);
                    break;
                case AuthCodes.eAuthCmd.CMD_AUTH_LOGON_PROOF:
                    resultOK = _HandleLogonProof(jsonNode);
                    break;
                case AuthCodes.eAuthCmd.CMD_AUTH_RECONNECT_CHALLENGE:
                    resultOK = _HandleReconnectChallenge(jsonNode);
                    break;
                case AuthCodes.eAuthCmd.CMD_AUTH_RECONNECT_PROOF:
                    resultOK = _HandleReconnectProof(jsonNode);
                    break;
                case AuthCodes.eAuthCmd.CMD_REALM_LIST:
                    resultOK = _HandleRealmList(jsonNode);
                    break;
                default:
                    break;
            }
        }

        if (!resultOK) {
            ObjectNode error = Json.newObject();
            error.put("error", 1);
            channel.write(error);
        }
    }

    public F.Callback0 closeCallback = new F.Callback0() {
        @Override
        public void invoke() throws Throwable {
            Logger.trace("closeCallback");
        }
    };

    public boolean _HandleLogonChallenge(JsonNode jsonNode) {
        boolean resultOK = false;

        if (jsonNode.get("error") != null && jsonNode.get("error").asInt() != ErrorCodes.SUCCESS) {
            ObjectNode error = Json.newObject();
            error.put("error", jsonNode.get("error").asInt());
            channel.write(error);
        }else {
            resultOK = true;
            AuthCodes.AuthResult authResult = AuthCodes.newAuthResult();
            authResult.cmd = 0;
            authResult.error = 0;

            ObjectNode result = Json.newObject();

            result.put("cmd", authResult.cmd);
            result.put("error", authResult.error);
            channel.write(result);
        }

        return resultOK;
    }

    public boolean _HandleLogonProof(JsonNode jsonNode) {
        boolean resultOK = false;

        if (jsonNode.get("error") != null && jsonNode.get("error").asInt() != ErrorCodes.SUCCESS) {
            ObjectNode error = Json.newObject();
            error.put("error", jsonNode.get("error").asInt());
            channel.write(error);
        }else {
            resultOK = true;
            AuthCodes.AuthResult authResult = AuthCodes.newAuthResult();
            authResult.cmd = 1;
            authResult.error = 0;
            authResult.gateway_add = "ws://172.16.1.66:7080";

            ObjectNode result = Json.newObject();

            result.put("cmd", authResult.cmd);
            result.put("error", authResult.error);
            result.put("gateway_add", authResult.gateway_add);

            channel.write(result);
        }

        return resultOK;
    }

    public boolean _HandleReconnectChallenge(JsonNode jsonNode) {
        boolean resultOK = false;
        return resultOK;
    }

    public boolean _HandleReconnectProof(JsonNode jsonNode) {
        boolean resultOK = false;
        return resultOK;
    }

    public boolean _HandleRealmList(JsonNode jsonNode) {
        boolean resultOK = false;
        return resultOK;
    }
}
