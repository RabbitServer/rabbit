package service;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import play.libs.Akka;
import play.libs.Json;
import play.mvc.WebSocket;
import scala.concurrent.Await;
import scala.concurrent.duration.Duration;

import static akka.pattern.Patterns.ask;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Created by Administrator on 2014/7/4.
 */
public class AuthActor extends UntypedActor {

    // Default AuthActor.
    static ActorRef defaultAuthActor = Akka.system().actorOf(Props.create(AuthActor.class));

    public static void realm(WebSocket.In<JsonNode> in, WebSocket.Out<JsonNode> out) throws Exception{

        AuthSocket callback = new AuthSocket(out);
        // Send the message to the realm
        String result = (String) Await.result(ask(defaultAuthActor, callback, 1000), Duration.create(1, SECONDS));

        if("OK".equals(result)) {

            // For each event received on the socket,
            in.onMessage(callback);

            // When the socket is closed.
            in.onClose(callback.closeCallback);

        } else {

            // Cannot connect, create a Json error.
            ObjectNode error = Json.newObject();
            error.put("error", result);

            // Send the error to the socket.
            out.write(error);

        }

    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof AuthSocket) {
            getSender().tell("OK", getSelf());
        }else {
            unhandled(message);
        }
    }
}
