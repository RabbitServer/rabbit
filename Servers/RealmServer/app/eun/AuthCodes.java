package eun;

/**
 * Created by Administrator on 2014/7/7.
 */
public class AuthCodes {

    private final static AuthCodes instance = new AuthCodes();
    /**
     * 认证API
     */
    public class eAuthCmd {
        public final static int CMD_AUTH_LOGON_CHALLENGE = 0x00; /**0x00 版本认证API*/
        public final static int CMD_AUTH_LOGON_PROOF  = 0x01; /**0x01 账号认证API*/
        public final static int CMD_AUTH_RECONNECT_CHALLENGE = 0x02; /**0x02 版本重新认证API*/
        public final static int CMD_AUTH_RECONNECT_PROOF = 0x03; /**0x03 账号重新认证API*/
        public final static int CMD_REALM_LIST = 0x10; /**0x10 获取服务端列表*/
    };

    /**
     * 向网关服请求API
     */
    public class eAuthSrvCmd {

    };

    /**
     * 登陆信息
     */
    public class AuthLogonChallenge {
        public int cmd;                /**cmd API接口*/
        public int error;              /**error 错误信息*/
        public String gamename;
        public int version1;
        public int version2;
        public int version3;
        public int build;
        public String platform;
        public String os;
        public String country;
        public Long timezone_bias;
        public String ip;
    };

    /**
     * 认证信息
     */
    public class AuthLogonProof {
        public int cmd;
        public int error;
        public String name;
        public String crc_hash;
        public Long timestamp;
    };

    /**
     * 返回结果
     */
    public class AuthResult {
        public int cmd;
        public int error;
        public String gateway_add;
    };

    private AuthResult AuthResultFactory() {
        AuthResult result = new AuthResult();
        return result;
    }

    public static AuthResult newAuthResult() {
        return instance.AuthResultFactory();
    }
}
