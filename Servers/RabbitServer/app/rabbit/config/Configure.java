package rabbit.config;

/**
 * Created by Administrator on 2014/8/1.
 */
public class Configure {

    static Configure instance = null;

    public final ServerURL HOST = new ServerURL("master", "127.0.0.1", 7050, 7050);
    public final ServerURL GATE_HOST[] = {new ServerURL("gate", "127.0.0.1", 7050, 7050)};
    public final ServerURL AUTH_HOST[] = {new ServerURL("auth", "127.0.0.1", 7050, 7050)};
    public final ServerURL CONNECTOR_HOST[] = {new ServerURL("connector", "127.0.0.1", 7050, 7050)};
    public final ServerURL AREA_HOST[] = {new ServerURL("area", "127.0.0.1", 7050, 7050)};

    static public Configure defaultConfigure() {
        if(instance == null)instance = new Configure();
        return  instance;
    }

    public class ServerURL {
        public String NAME = "TEST";
        public String HOST = "127.0.0.1";
        public int LOCAL = 7050;
        public int PORT = 0;

        public ServerURL(String name, String host, int localPort, int publicPort) {
            this.NAME = name;
            this.HOST = host;
            this.LOCAL = localPort;
            this.PORT = publicPort;
        }
    }
}
