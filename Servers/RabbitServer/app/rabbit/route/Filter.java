package rabbit.route;

import play.Logger;
import play.libs.F;
import play.mvc.WebSocket;
import rabbit.support.protocol.RabbitPackage;

/**
 * Created by Administrator on 2014/7/31.
 */
public class Filter implements F.Callback<byte[]> {

    public Long uid;
    public String sid;
    public WebSocket.Out<byte[]> out;

    public Filter(WebSocket.Out<byte[]> socketOut) {
        // Generate a unique id
        this.sid = java.util.UUID.randomUUID().toString();
        this.out = socketOut;
    }

    @Override
    public void invoke(byte[] bytes) throws Throwable {
        RabbitPackage.Package pkg = RabbitPackage.decode(bytes);
        switch (pkg.getType()) {
            case RabbitPackage.TYPE_HANDSHAKE:
                Route.defaultRoute().accept(pkg.getBody());
                break;
            case RabbitPackage.TYPE_HANDSHAKE_ACK:
                break;
            case RabbitPackage.TYPE_HEARTBEAT:
                break;
            case RabbitPackage.TYPE_DATA:
                break;
            case RabbitPackage.TYPE_KICK:
                break;
            default:
                Logger.info("error package type : " + pkg.getType());
                break;
        }
        //Logger.info("onMessage : " + msg.getBodyJson().toString());
        // Cannot connect, create a Json error.
        //byte[] result = RabbitPackage.encode(RabbitPackage.TYPE_HANDSHAKE, RabbitPackage.strencode("{\"code\":200,\"sys\":{}}"));
        // Send the error to the socket.
        //emit(result);
    }

    public F.Callback0 closeCallback = new F.Callback0() {
        @Override
        public void invoke() throws Throwable {
            Logger.trace("closeCallback");
            uid = null;
            sid = null;
            out = null;
        }
    };

    public void emit(byte[] bytes) {
        if (out != null) out.write(bytes);
    }
}
