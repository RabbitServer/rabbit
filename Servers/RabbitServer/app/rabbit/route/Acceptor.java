package rabbit.route;

import play.mvc.WebSocket;

/**
 * Created by Administrator on 2014/7/31.
 */
public class Acceptor {
    /**
     * Join the default room.
     */
    static public void accept(WebSocket.In<byte[]> in, WebSocket.Out<byte[]> out) throws Exception{

        // make a session

        Filter filter = new Filter(out);

        // For each event received on the socket,
        in.onMessage(filter);

        // When the socket is closed.
        in.onClose(filter.closeCallback);

    }
}
