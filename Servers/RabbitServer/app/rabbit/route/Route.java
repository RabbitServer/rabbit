package rabbit.route;

import rabbit.config.Configure;
import rabbit.config.Configure.ServerURL;
import rabbit.support.protocol.RabbitMessage;

/**
 * Created by Administrator on 2014/7/31.
 */
public class Route {

    static Route instance = null;

    static public Route defaultRoute() {
        if(instance == null)instance = new Route();
        return  instance;
    }

    public Route()
    {
    }

    public void accept(byte[] bytes) {
        RabbitMessage.Message msg = RabbitMessage.decode(bytes);
        String address[] = msg.getRoute().split(".");
        if (address.length == 3) {
            ServerURL sURL = findServerURL(address[0]);
            if (sURL.PORT == 0) {
                forward(sURL.HOST, sURL.LOCAL, 0, bytes);
            }else {
                forward(sURL.HOST, sURL.PORT, 0, bytes);
            }
        }

//        switch (msg.getType()) {
//            case RabbitMessage.TYPE_REQUEST:
//                break;
//            case RabbitMessage.TYPE_NOTIFY:
//                break;
//            case RabbitMessage.TYPE_RESPONSE:
//                break;
//            case RabbitMessage.TYPE_PUSH:
//                break;
//            default:
//                Logger.info("error package type : " + msg.getType());
//                break;
//        }
    }

    public void forward(String server, int port, int func, byte[] bytes) {

    }

    public ServerURL findServerURL(String serverName) {
        if (serverName == Configure.defaultConfigure().HOST.NAME)return Configure.defaultConfigure().HOST;
        for (ServerURL url: Configure.defaultConfigure().GATE_HOST) {
            if (url.NAME.indexOf(serverName) == 0)return url;
        }
        for (ServerURL url: Configure.defaultConfigure().AUTH_HOST) {
            if (url.NAME.indexOf(serverName) == 0)return url;
        }
        for (ServerURL url: Configure.defaultConfigure().CONNECTOR_HOST) {
            if (url.NAME.indexOf(serverName) == 0)return url;
        }
        for (ServerURL url: Configure.defaultConfigure().AREA_HOST) {
            if (url.NAME.indexOf(serverName) == 0)return url;
        }
        return null;
    }
}
