package controllers;

import play.mvc.Controller;
import play.mvc.WebSocket;
import rabbit.route.Acceptor;

public class Application extends Controller {


    /**
     * Handle the chat web socket.
     */
    public static WebSocket<byte[]> index() {
        return new WebSocket<byte[]>() {

            // Called when the Web socket Handshake is done.
            public void onReady(WebSocket.In<byte[]> in, WebSocket.Out<byte[]> out){

                // Join the chat room.
                try {
                    Acceptor.accept(in, out);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };
    }

}
