# Rabbit -- README

## Welcome to Rabbit

Rabbit is a free project with the following goal:

  * This project is about developing a server software that is able to
    MMORPG service.
  * This project is focused on developing content!
  * Also there are many other aspects that need to be done and are
    considered equally important.
  * Anyone who wants to do stuff is very welcome to do so!
  * Our goal must always be to provide the best code that we can.
  * Being 'right' is defined by the behavior of the system
    we want to emulate.
  * Developing things right also includes documenting and discussing
    _how_ to do things better, hence...
  * Learning and Teaching are very important in our view, and must
    always be part of what we do.
  * of our work: Our work - including our code - is released under the GPL
    So everbody is free to use and contribute to this open source project
  * for our developers and contributers on things that interest them.
    No one here is telling anybody _what_ to do.
    If you want somebody to do something for you, pay him,
    but we are here to enjoy.
  * to have FUN with developing.

## License

  Rabbit is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

  You can find the full license text in the file [COPYING](COPYING) delivered with this package.
